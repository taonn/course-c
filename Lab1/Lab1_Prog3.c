#include <stdio.h>
#define PI 3.141592654

int main() {
	float angle;
	char sys;
	printf("Enter the angle, please (60.00D)\n");
	scanf("%f%c", &angle, &sys);
	if ((sys == 'D') || (sys == 'd')) { printf("%.2fR\n", PI*(angle/180)); }
	else if ((sys == 'R') || (sys == 'r')) { printf("%.2fD\n", 180*(angle/PI)); }
	else { printf("Wrong input!\n"); return 1; }
	return 0;
}