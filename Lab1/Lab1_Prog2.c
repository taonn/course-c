#include <stdio.h>
#define NIGHT 0
#define MORNING 5
#define DAY 12
#define EVENING 18

void printTime(int hour, int minute) {
	if ((hour < 10) && (minute < 10)) {	printf("Actual time is 0%d:0%d\n", hour, minute); }
	if ((hour < 10) && (minute > 9)) { printf("Actual time is 0%d:%d\n", hour, minute); }
	if ((hour > 9) && (minute < 10)) { printf("Actual time is %d:0%d\n", hour, minute); }
	if ((hour > 9) && (minute > 9)) { printf("Actual time is %d:%d\n", hour, minute); }
}

void printHello(int hour) {
	if ((hour >= NIGHT) && (hour < MORNING)) {	printf("Good night!\n"); }
	else if ((hour >= MORNING) && (hour < DAY)) { printf("Good morning!\n"); }
	else if ((hour >= DAY) && (hour < EVENING)) { printf("Good aftenoon!\n"); }
	else { printf("Good evening!\n"); }
}

int main() {
	int hour, minute;
	printf("What time is it? (hh:mm)\n");
	scanf("%d:%d", &hour, &minute);
	
	if (hour == 24) { hour = 0; }
	if ((hour > 24) || (hour < 0) || (minute < 0)) {
		printf("Time is incorrect!\n");
		return 1;
	}
	if (minute > 59) {
		printf("You have entered 60 (or more) minutes! Time will be recalculated!\n\n");
		hour += minute/60;
		minute = minute%60;
		printTime(hour, minute);
	}
	
	printHello(hour);

	return 0;
}