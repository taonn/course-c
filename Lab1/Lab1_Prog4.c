#include <stdio.h>
#define FOOT 12.00
#define INCH 2.54

int main() {
	float foots, inches;
	char ftin;
	printf("Enter your height, please (2f3i)\n");
	scanf("%f%c%f%c", &foots, &ftin, &inches, &ftin);
	printf("Your height is %.1fcm\n", INCH*(foots*FOOT+inches));
	return 0;
}