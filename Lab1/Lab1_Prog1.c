#include <stdio.h>
#define MAN_FAT 0.5
#define MAN_SLIM 0.4
#define WOMAN_FAT 0.4
#define WOMAN_SLIM 0.3

int main () {
	char gender;
	int height, weight;
	printf("Enter your gender (f/m)\n");
	scanf("%c", &gender);
	//if (gender == 'f') printf("Your gender is female\n\n");
	//else if (gender == 'm') printf("Your gender is male\n\n");
	//else { printf("Your gender is incorrect!\n\n"); return 1; }
	if ((gender != 'f') && (gender != 'm')) { 
		printf("Your gender is incorrect!\n\n");
		return 1;
	}
	else { printf("\n"); }
	
	printf("Enter your height (cm)\n");
	scanf("%d", &height);
	printf("\n");
	//printf("Your height is %d cm\n\n", height);

	printf("Enter your weight (kg)\n");
	scanf("%d", &weight);
	printf("\n");
	//printf("Your weight is %d kg\n\n", weight);

	if (gender == 'm') {
		if ((float) weight/height > MAN_FAT) printf("You need to lose some weight!");
		else if ((float) weight/height < MAN_SLIM) printf("You need to get some weight!");
		else printf("You are ok!");
	}

		if (gender == 'f') {
		if ((float) weight/height > WOMAN_FAT) printf("You need to lose some weight!\n");
		else if ((float) weight/height < WOMAN_SLIM) printf("You need to get some weight!\n");
		else printf("You are ok!\n");
	}
		printf("\n");
	return 0;
}