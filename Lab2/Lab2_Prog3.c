#include <stdio.h>

int main() {
	int num, i, j;
	printf("Enter number of strokes:\n");
	scanf("%d", &num);
	for (i = 1; i < num+1; i++) {
		for (j = 0; j < num - i; j++) {
			putchar(' ');
		}
		for (j = 0; j < 2*i-1; j++) {
			putchar('*');
		}
		putchar('\n');
	}
	return 0;
}