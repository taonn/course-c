#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define NUM_START 47
#define NUM_END 58

int main() {
	char str[256] = "asdasd3df65a1sfbasf132a8adsf4basdf352b1ads3fb10asd58asd3ga4as984af321";
	int i, j;
	char bin;
	printf("Original string: %s\n", str);
	for (i = 0; i < strlen(str); i++) {
		if (str[i] > NUM_START && str[i] < NUM_END) { 
			j = i;
			while (str[j] > NUM_START && str[j] < NUM_END && j < strlen(str)-1) { 
				j++;
			}
			if (str[j] > NUM_START && str[j] < NUM_END) continue;
				bin = str[i];
				str[i] = str[j];
				str[j] = bin;
			}
	}
	printf("  Sorted string: %s\n", str);

	return 0;
}