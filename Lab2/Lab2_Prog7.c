#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
	char str[255];
	char num[255] = {0};
	int i;
	printf("Enter your text, please:\n");
	fflush(stdin);
	fgets(str, 256, stdin);
	str[strlen(str) - 1] = '\0'; // delete \n
	for (i = 0; i < strlen(str); i++) { num[str[i]]++; } // counter
	for (i = 0; i < 255; i++) {	// printer
		if (num[i] != 0) { 
			printf("%c : %d\n", i, num[i]); 
		} 
	}
	return 0;
}