#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
	int num = 0;
	int guess = 0;
	char done = 0;
	int attempts = 0;
	srand(time(NULL));
	num = rand()%100 + 1;
	while (done == 0) {
		printf("Enter you guess:\n");
		scanf("%d", &guess);
		attempts++;
		if (num == guess) { 
			printf("You are right! Well done! Number of attempts = %d\n", attempts);
			done = 1;
		}
		else if (num > guess) { 
			printf("Your number is less than my! Try again!\n");
		}
		else {

			printf("Your number is more than my! Try again!\n");
		}
	}
	return 0;
}