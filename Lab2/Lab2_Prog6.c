#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
	char str[256];
	char mid;
	int i, j, num;
	printf("Enter youe text, please:\n");
	fflush(stdin);
	fgets(str, 256, stdin);

	str[strlen(str)-1] = '\0'; // delete \n from the end

	while (str[0] == ' ') { // delete first spaces
		for (i = 0; i < strlen(str); i++) {	str[i] = str[i+1]; }
	}

	for (i = 0; i < strlen(str); i++) { // delete middle spaces
		if (str[i] == ' ' && str[i + 1] == ' ') {
			for (j = i; j < strlen(str); j++) {	str[j] = str[j+1]; }
			i--;
		}
	}

	if (str[strlen(str) - 1] == ' ') { str[strlen(str) - 1] = '\0'; } // delete last space

	printf("%s\n", str);
	return 0;
}