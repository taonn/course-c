#include <stdio.h>
#include <string.h>

void printSeq(char* start) {
	char chk = *start;
	int num = 0;
	while (*start == chk) {
		putchar(*start);
		num++;
		start++;
	}
	printf(" - %d\n", num);
}

int main() {
	char str[256];
	char* seq;
	char* strA;
	int len;
	int i, j, numA = 0, numStore = 0;

	printf("Enter string\n");
	fgets(str, 256, stdin);
	len = strlen(str) - 1;

	for (i = 0; i < len; i++) {
		strA = &str[i];
		numA = 1;
		while (str[i + 1] == *strA) {
			numA++;
			i++;
		}
		if (numA > numStore) {
			numStore = numA;
			seq = &str[i - numA + 1];
		}
	}
	printSeq(seq);
	return 0;
}