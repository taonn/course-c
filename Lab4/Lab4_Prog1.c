#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void print(char* str) {
	while (*str != '\0' && *str != '\n') { 
		putchar(*str);
		*str++;
	}
	putchar('\n');
}

int main() {
	char strArr[255][255];
	char* strPointer[255];
	char* strMid;
	int i = 0;
	int j, k, indexMax, lengthMax;
	printf("Enter stroke, please\n");
	fflush(stdin);
	fgets(strArr[i], 255, stdin);
	if (strlen(strArr[i]) == 1) { printf("Empty string! Input stopped!\n"); return 0; }
	strPointer[i] = strArr[i];
	while (strlen(strArr[i]) > 1) {
		fflush(stdin);
		i++;
		fgets(strArr[i], 255, stdin);
		if (strlen(strArr[i]) == 1) { printf("Empty string! Input stopped!\n"); break; }
		strPointer[i] = strArr[i];
	}
	printf("\n..Sorting input strings by length..\n\n");

	for (j = 0; j < i; j++) {
		indexMax = j;
		lengthMax = strlen(strPointer[j]);
		for (k = j; k < i; k++) {
			if (strlen(strPointer[k]) > lengthMax) { 
				indexMax = k; 
				lengthMax = strlen(strPointer[k]);
			}
		}
		if (indexMax != j) {
			strMid = strPointer[j];
			strPointer[j] = strPointer[indexMax];
			strPointer[indexMax] = strMid;
		}
	}

	for (j = 0; j < i; j++) {
		print(strPointer[j]);
	}

	return 0;
}