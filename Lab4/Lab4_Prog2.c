#include <stdio.h>
#include <string.h>

void printWord(char* word) {
	while ((*word != ' ') && (*word != '\n')) {
		printf("%c", *word);
		word++;
	}
}

int main() {
	char str[256];
	char* words[256];
	int len, i, j, id = 0;
	printf("Enter string\n");
	fflush(stdin);
	fgets(str, 256, stdin);
	len = strlen(str);

	if ((str[0] != ' ') && (str[0] != '\n')) { 
		words[id] = &str[0]; 
		id++; 
	}

	for (i = 1; i < len; i++) {
		if (((str[i - 1] == ' ') || (str[i - 1] == '\n')) && ((str[i] != ' ') && (str[i] != '\n'))) {
			words[id] = &str[i]; 
			id++;
		}
	}

	for (i = id-1; i >=0; i--) {
		printWord(words[i]);
		putchar(' ');
	}
	putchar('\n');

	return 0;
}