#include <stdio.h>
#include <string.h>

int main() {
	char str[256];
	int len, i, j;
	printf("Enter string\n");
	fflush(stdin);
	fgets(str, 256, stdin);
	str[strlen(str) - 1] = '\0';
	len = strlen(str)-1;

	// delete all spaces
	for (i = 0; i < len; i++) {
		if (str[i] == ' ') {
			for (j = i; j < len; j++) {
				str[j] = str[j + 1];
			}
			str[len] = 0;
			len--;
			i--;
		}
	}
	// delete the last space
	if (str[len] == ' ') {
	str[len] = 0;
	len--;
	}
	puts(str);

	for (i = 0; i < len/2; i++) {
		if (str[i] != str[len - i]) {
			printf("String is not a polyndrom! - \n");
			return 0;
		}
	}
	printf("String is a polyndrom! + \n");
	return 0;
}