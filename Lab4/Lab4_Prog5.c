#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main() {
	char strArr[255][255];
	char* strPointer[255];
	char* strMid;
	int i = 0, j, k, indexMax, lengthMax;
	FILE* fcp = fopen("output.txt","w");
	FILE* fp = fopen("input.txt","rt");
	if (!fp) { 
		printf("Error! Can't open file!\n");
		return 1;
	}
	// input
	while(!feof(fp)) {
		fgets(strArr[i], 255, fp);
		strPointer[i] = strArr[i];
		i++;
	}
	fclose(fp);

	// sorting
	for (j = 0; j < i; j++) {
		indexMax = j;
		lengthMax = strlen(strPointer[j]);
		for (k = j; k < i; k++) {
			if (strlen(strPointer[k]) > lengthMax) { 
				indexMax = k; 
				lengthMax = strlen(strPointer[k]);
			}
		}
		if (indexMax != j) {
			strMid = strPointer[j];
			strPointer[j] = strPointer[indexMax];
			strPointer[indexMax] = strMid;
		}
	}
	// output
	for (j = 0; j < i; j++) {
		fprintf(fcp, "%3d - %s", strlen(strPointer[j]), strPointer[j]);
	}
	fclose(fcp);

	return 0;
}