#include <stdio.h>
#include <string.h>

int main() {
	char fam[256][256];
	char* young;
	char* old;
	int iyoung = 200, iold = 0;
	int number, i, age;
	printf("How many family members are there?\n");
	fflush(stdin);
	scanf("%d", &number);
	for (i = 0; i < number; i++) {
		printf("Enter your %d family members name\n", i + 1);
		fflush(stdin);
		fgets(fam[i],256, stdin);
		fam[i][strlen(fam[i]) - 1] = 0;
		printf("Enter his/hers age\n");
		fflush(stdin);
		scanf("%d", &age);
		if (age > iold) {
			old = fam[i];
			iold = age;
		}
		if (age < iyoung) {
			young = fam[i];
			iyoung = age;
		}
	}
	printf("%s - is youngest\n", young);
	printf("%s - is oldest\n", old);
	return 0;
}