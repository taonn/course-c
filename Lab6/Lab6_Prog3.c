#include <stdio.h>
#include <time.h>

double fib(double prev1, double prev2, int N) {
	if ((N == 1.0) || (N == 0.0)) { return 1; }
	return prev1 + prev2;
}

int main() {
	int N, i;
	double fibonacci1, fibonacci2, fibonacci;
	clock_t begin, end;
	FILE *fw = fopen("output.csv", "wt");
	printf("Enter length of Fibonacci's array\n");
	scanf("%d", &N);
	fibonacci1 = 1.0;
	fibonacci2 = 1.0;
	for (i = 0; i < N + 1; i++) {
	begin = clock();
	fibonacci = fib(fibonacci1, fibonacci2, i);
	fibonacci1 = fibonacci2;
	fibonacci2 = fibonacci;
	end = clock();
	printf("%d - %.f - %.1fsec\n", i, fibonacci, (end - begin)/(double)CLOCKS_PER_SEC);
	fprintf(fw, "%d;%f;%f\n", i, fibonacci, (end - begin)/(double)CLOCKS_PER_SEC);
	}
	fclose(fw);
	return 0;
}