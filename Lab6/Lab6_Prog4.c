#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

static int str;
static int col;
static char gotPath;

void delay(double N) {
	clock_t start, end;
	start = clock();
	end = clock();
	while ((end - start)/(double)CLOCKS_PER_SEC < N) {
		end = clock();
	}
}

int g(int x, int y) { // recalculate coords
	return (x + col*y);
}

void printGot() { // got the path? print it!
	if (gotPath == 1) {
		printf("Got path! \n");
	}
	else {
		printf("Searching...\n");
	}
	return;
}

void printLab(char *lab) { // print whole lab
	int i, j;
	delay(0.02);
	system("cls");
	for (j = 0; j < str; j++) {
		for (i = 0; i < col; i++) {
			putchar(lab[g(i, j)]);
		}
		putchar('\n');
	}
	printGot();
}

void getPath(int x, int y, char *lab) { // searching exit
	if (gotPath == 1) {	return;	}
	if ((x == 0) || (x == col - 1) || (y == 0) || (y == str - 1)) {
		lab[g(x, y)] = 'X';
		gotPath = 1;
		return;
	}
	lab[g(x, y)] = 'x';
	printLab(lab);
	if (lab[g(x - 1, y)] == ' ') {  // going left
		lab[g(x, y)] = '*'; 
		getPath(x - 1, y, lab); 
		if (gotPath != 1) {
			lab[g(x - 1, y)] = '.'; 
		}
	}
	if (lab[g(x, y - 1)] == ' ') { // going up
		lab[g(x, y)] = '*'; 
		getPath(x, y - 1, lab); 
		if (gotPath != 1) {
			lab[g(x, y - 1)] = '.'; 
		}
	}
	if (lab[g(x + 1, y)] == ' ') { // going right
		lab[g(x, y)] = '*'; 
		getPath(x + 1, y, lab); 
		if (gotPath != 1) {
			lab[g(x + 1, y)] = '.'; 
		}
	}
	if (lab[g(x, y + 1)] == ' ') { // going down
		lab[g(x, y)] = '*'; 
		getPath(x, y + 1, lab); 
		if (gotPath != 1) {
			lab[g(x, y + 1)] = '.'; 
		}
	}
	return;
}

int main() {
	FILE *fIn;
	char lab[256];
	char labLin[256*256];
	int i;
	int beginX = -1, beginY = -1;
	col = 0, str = 0;
	srand(NULL);
	fIn = fopen("lab.txt", "rt");
	while (fgets(lab, 256, fIn) != NULL) {
		if (col == 0) { col = strlen(lab) - 1; }
		if (beginX == -1) {
		for (i = 0; i < col; i++) {
			if (lab[i] == 'X' || lab[i] == 'x') {
				beginX = i;
				beginY = str;
			}
		}
		}
		for (i = 0; i < col; i++) { // write readed line to linear lab array
			labLin[g(i, str)] = lab[i];
		}
		str++;
	}
	fclose(fIn);
	gotPath = 0;
	printLab(labLin);
	printf("x: %d; y: %d\n", beginX, beginY);
	printf("col: %d; str: %d\n", col, str);
	delay(1);
	getPath(beginX, beginY, labLin);
	if (gotPath == 1) { printLab(labLin); } // resume
	else { printf("Path not found!\n"); }

	return 0;
}