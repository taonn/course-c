#include <stdio.h>
#include <time.h>

long fib(int N) {
	if ((N == 1) || (N == 0)) { return 1; }
	return fib(N - 1) + fib(N - 2);
}

int main() {
	int N, i;
	long fibonacci;
	clock_t begin, end;
	FILE *fw = fopen("output.csv", "wt");
	printf("Enter length of Fibonacci's array\n");
	scanf("%d", &N);
	for (i = 0; i < N + 1; i++) {
	begin = clock();
	fibonacci = fib(i);
	end = clock();
	printf("%d - %d - %.1fsec\n", i, fibonacci, (end - begin)/(double)CLOCKS_PER_SEC);
	fprintf(fw, "%d;%f\n", i, (end - begin)/(double)CLOCKS_PER_SEC);
	}
	fclose(fw);
	return 0;
}