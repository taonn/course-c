#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int sumNorm (int* arr, int begin, int end) { // sum of array in normal mode
	int i, sum = 0;
	for (i = begin; i < end; i++) {
		sum += arr[i];
	}
	return sum;
}

int sumRecursive (int* arr, int begin, int end) { // sum of array in normal mode
	if (end - begin <= 1) { return arr[begin] + arr[end]; }
	return sumRecursive(arr, begin, (end + begin)/2) + sumRecursive(arr, (end + begin)/2 + 1, end);
}

void fillArr(int* arr, int begin, int end) { // fiiling array with random numbers
	int i;
	srand(time(NULL));
	for (i = begin; i < end; i++) {
		arr[i] = rand()%100;
	}
}

int main() {
	int N, lenArr;
	int sum1, sum2;
	int* arr;
	clock_t start, finish;
	printf("Enter N\n");
	scanf("%d", &N);
	lenArr = (int) pow(2,N);
	arr = (int*)malloc(lenArr*sizeof(int));
	
	while (!arr) {
		printf("Too big N, enter smaller value N\n");
		scanf("%d", &N);
		lenArr = (int) pow(2,N);
		arr = (int*)malloc(lenArr*sizeof(int));
	}
	
	fillArr(arr, 0, lenArr);

	start = clock();
	sum1 = sumNorm(arr, 0, lenArr);
	finish = clock();
	printf("Traditional method: %d - %d clocks\n", sum1, finish - start);

	start = clock();
	sum2 = sumRecursive(arr, 0, lenArr - 1);
	finish = clock();
	printf("Recursive method:   %d - %d clocks\n", sum2, finish - start);

	free(arr);
	return 0;
}