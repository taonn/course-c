#include <stdio.h>
#include <string.h>
#include <stdlib.h>


char partition(char *buf, char *expr1, char *expr2) {
	char *run = buf;
	char *end;
	char *op = 0;
	int brackets = 0;
	int i;
	//puts("partition");
	//puts(buf);
	while (*run != 0) {
		if (*run == '(') { brackets++; }
		if (*run == ')') { brackets--; }
		if ((*run == '+' || *run == '-' || *run == '*' || *run == '/') && (brackets == 1)) {
			op = run;
		}
		run++;
	}
	//puts("op");
	//putchar(*op);
	//putchar('\n');
	end = run;

	run = buf + 1;
	i = 0;
	while (run < op) {
		expr1[i] = *run;
		i++;
		run++;
	}
	expr1[i] = '\0';
	//puts("expr1");
	//puts(expr1);

	run = op + 1;
	i = 0;
	while (run < end - 1) {
		expr2[i] = *run;
		i++;
		run++;
	}
	expr2[i] = '\0';
	//puts("expr2");
	//puts(expr2);
	return *op;
}

double eval(char *buf) {
	char op = 0;
	char expr1[256],expr2[256];
	//puts("eval");
	//puts(buf);
	if(*buf != '(') { return atoi(buf); }
	op = partition(buf, expr1, expr2);
	switch(op)
	{
	case '+':
	return eval(expr1) + eval(expr2);
	case '-':
	return eval(expr1) - eval(expr2);
	case '*':
	return eval(expr1) * eval(expr2);
	case '/':
	return eval(expr1) / eval(expr2);
	}
}


int main(int argc, char *argv[]) {
	double answer;
	puts("Expression entered:");
	puts(argv[1]);
	answer = eval(argv[1]);
	printf("Answer is: %.3f\n", answer);
	return 0;
}