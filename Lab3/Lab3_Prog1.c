#include <stdio.h>
#include <string.h>

int main() {
	char str[255];
	int i, j;
	int words = 0;
	printf("Enter string\n");
	fflush(stdin);
	fgets(str, 255, stdin);
	str[strlen(str) - 1] = '\0';
	//puts(str);

	for (i = 0; i < strlen(str); i++) {
		if (str[i] != ' ') {
			j = i;
			while (str[j] != ' ' && str[j] != '\0' && str[j] != '\n') {
				putchar(str[j]);
				j++;
			}
			printf(" : %d symbols\n", j-i);
			words++;
			i = j;
		}
	}
	printf("%d words\n", words);
	return 0;
}