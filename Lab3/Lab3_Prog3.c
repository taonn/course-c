#include <stdio.h>
#include <string.h>

#define NUML 3

int summ(int num[], int len) {
	int i;
	int sum = 0;
	int ten = 1;
	for (i = len - 1; i >= 0; i--) {
		sum += ten*num[i];
		ten = 10*ten;
	}
	return sum;
}

int main() {
	char num[255];
	int numl[NUML];
	int i, len = 0;
	int sum = 0;
	printf("Enter string\n");
	fflush(stdin);
	fgets(num, 255, stdin);

	for (i = 0; i < strlen(num); i++) {
		if (num[i] >= '0' && num[i] <= '9') {
			numl[len] = num[i]-'0';
			len++;
		}
		if (num[i] < '0' || num[i] > '9' || len == NUML)  {
			sum += summ(numl, len);
			len = 0;
		}
	}

	printf("%d\n", sum);
	return 0;
}