#include <stdio.h>
#include <string.h>

int main() {
	char str[255];
	int i, j, k, len;
	int words = 0;
	int word = 0;
	printf("Enter string\n");
	fflush(stdin);
	fgets(str, 255, stdin);
	printf("Enter the number of word to delete\n");
	fflush(stdin);
	scanf("%d", &word);
	len = strlen(str) - 1;

	for (i = 0; i < len; i++) {
		if (str[i] != ' ') {
			j = i;
			while (str[j] != ' ' && str[j] != '\0' && str[j] != '\n') {
				j++;
			}
			words++;
			if (word == words) {
				for (k = j; k < len; k++) {
					str[k - (j - i) - 1] = str[k];
				}
				len -= j - i;
				str[len] = 0;
			}
			i = j;
		}
	}
	if (words < word) {
		printf("There is no such word!\n");
		return 1;
	}
	puts(str);
	return 0;
}