#include <stdio.h>
#include <string.h>

int main() {
	char str[256];
	char strA, strStore;
	int len;
	int i, j, numA = 0, numStore = 0;
	printf("Enter string\n");
	fgets(str, 256, stdin);
	len = strlen(str) - 1;
	for (i = 0; i < len; i++) {
		strA = str[i];
		numA = 1;
		while (str[i + 1] == strA) {
			numA++;
			i++;
		}
		if (numA > numStore) {
			numStore = numA;
			strStore = strA;
		}
	}
	printf("'%c' - %d times\n", strStore, numStore);
	for (i = 0; i < numStore; i++) {
		putchar(strStore);
	}
	putchar('\n');
	return 0;
}