#include <stdio.h>
#include <string.h>

int main() {
	char str[255];
	int indexMax, i, j;
	int lengthMax = 0;
	printf("Enter string\n");
	fflush(stdin);
	fgets(str, 255, stdin);
	str[strlen(str) - 1] = '\0';

	for (i = 0; i < strlen(str); i++) {
		if (str[i] != ' ' && str[i] != '\n' && str[i] != '\0') {
			j = i;
			while (str[j] != ' ' && str[j] != '\n' && str[j] != '\0') {
				j++;
			}
			if (j - i > lengthMax) {
				lengthMax = j - i;
				indexMax = i;
			}
			i = j;
		}
	}

	for (i = 0; i < strlen(str); i++) {
		if (i >= indexMax && i < indexMax + lengthMax) {
			putchar(str[i]);
		}
		else putchar(' ');
	}
	putchar('\n');
	for (i = 0; i < strlen(str); i++) {
		if (i == indexMax) {
			printf("%d", lengthMax);
		}
		else putchar(' ');
	}
	putchar('\n');
	return 0;
}