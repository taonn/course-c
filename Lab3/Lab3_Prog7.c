#include <stdio.h>
#include <string.h>

int main() {
	char str[255];
	int i, j, k;
	int words = 0;
	int word = 0;
	printf("Enter string\n");
	fflush(stdin);
	fgets(str, 255, stdin);
	printf("Enter the printout word number\n");
	fflush(stdin);
	scanf("%d", &word);
	str[strlen(str) - 1] = '\0';
	//puts(str);

	for (i = 0; i < strlen(str); i++) {
		if (str[i] != ' ') {
			j = i;
			while (str[j] != ' ' && str[j] != '\0' && str[j] != '\n') {
				j++;
			}
			words++;
			if (word == words) {
				for (k = i; k < j; k++) {
					putchar(str[k]);
				}
				putchar('\n');
			}
			i = j;
		}
	}
	if (words < word) {
		printf("There is no such word!\n");
		return 1;
	}
	return 0;
}