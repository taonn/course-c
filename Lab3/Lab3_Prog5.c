#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
	int N = 0, i;
	int iMin = -1;
	int iMax = -1;
	int max = -501;
	int min = +501;
	int sum = 0;
	int nArr[256] = {0};
	srand(time(NULL));
	printf("Enter N (max 256)\n");
	scanf("%d", &N);
	// fill array with random numbers
	for (i = 0; i < N; i++) { nArr[i] = rand()%1001 - 500; }
	// find min and max
	for (i = 0; i < N; i++) {
		if (nArr[i] < min) { iMin = i; min = nArr[i]; } // get min
		if (nArr[i] > max) { iMax = i; max = nArr[i]; } // get max
	}
	// sum the array
	if (iMax < iMin) { 
		for (i = iMax; i < iMin + 1; i++) {	sum += nArr[i];	}
	}
	else {
		for (i = iMin; i < iMax + 1; i++) {	sum += nArr[i];	}
	}
	printf("Sum between first negative and last positive: %d\n", sum);
	return 0;
}