#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
	int N = 0, i;
	int iPos = -1;
	int iNeg = -1;
	int sum = 0;
	int nArr[256] = {0};
	srand(time(NULL));
	printf("Enter N (max 256)\n");
	scanf("%d", &N);
	// fill array with random numbers
	for (i = 0; i < N; i++) { nArr[i] = rand()%1001 - 500; }
	// find first negative and last positive
	for (i = 0; i < N; i++) {
		if ((nArr[i] < 0) && (iNeg == -1)) { iNeg = i; } // get index of first negative number
		if ((nArr[N - 1 - i] > 0) && (iPos == -1)) { iPos = N - 1 - i; } // get index of last positive number
	}
	// check it
	if ((iPos == -1) || (iNeg == -1)) {
		printf("There is no suitable number\n");
		return 1;
	}
	// sum the array
	for (i = iNeg; i < iPos + 1; i++) {	sum += nArr[i];	}
	printf("Sum between first negative and last positive: %d\n", sum);
	return 0;
}