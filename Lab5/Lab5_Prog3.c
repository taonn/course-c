#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

void mutateWord(FILE *fw, char *str) {
	char word[30];
	char mid;
	int len = strlen(str);
	int i, j, k;
	int idMid;
	srand(time(NULL));
	for (i = 0; i < len; i++) {
		if (str[i] != ' ') {
			j = i;
			if (str[j] != ' ' && str[j] != '\0' && str[j] != '\n') { // get into the word
			while (str[j] != ' ' && str[j] != '\0' && str[j] != '\n') {
				word[j - i] = str[j]; // construct the word
				j++;
			}
			fputc(word[0], fw); // print first letter
			for (k = 1; k < j - i - 1; k++) { // mutate the word and output into file
				idMid = rand()%(j - i - k - 1) + k;
				mid = word[idMid];
				word[idMid] = word[k];
				word[k] = mid;
				fputc(word[k], fw);
			}
			if (j - i > 1) { fputc(word[k], fw); } // print last letter
			fputc(' ', fw);
			}
			i = j;
		}
	}
	fputc('\n', fw);
}

int main() {
	char str[1000];
	int ch = 0;
	int len = 0;
	FILE *fp = fopen("input.txt", "rt");
	FILE *fw = fopen("output.txt", "wt");
	while (	fgets(str, 1000, fp) != NULL) {
		mutateWord(fw, str);
	}
	fclose(fp);
	fclose(fw);
	return 0;
}