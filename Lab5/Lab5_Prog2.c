#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define HEIGHT 10
#define WIDTH 10

int getX (int i, int j) {
	int x;
	x = WIDTH*i + j;
	return x;
}

void clearArr(char* ch) {
	int i, j;
	// clear array
	for (i = 0; i < HEIGHT; i++) {
		for (j = 0; j < WIDTH; j++) {
			ch[getX(i, j)] = ' ';
		}
	}
}

void fillArr(char* ch) {
	int i, j;
	char cha;
	srand(clock());
	// fill quarter of array with stars
	for (i = 0; i < HEIGHT/2; i++) {
		for (j = 0; j < WIDTH/2; j++) {
			cha = rand()%2;
			if (cha == 1) { ch[getX(i, j)] = '*'; }
		}
	}
	// copying stars to another quarters
	for (i = 0; i < HEIGHT/2; i++) {
		for (j = 0; j < WIDTH/2; j++) {
						 ch[getX(i, WIDTH - j - 1)] = ch[getX(i, j)];
			ch[getX(HEIGHT - i - 1, WIDTH - j - 1)] = ch[getX(i, j)];
			ch[getX(HEIGHT - i - 1, j)] = ch[getX(i, j)];
		}
	}
}

void printArr(char* ch) {
	int i, j;
	// clear screen
	system("cls");
	// print array
	for (i = 0; i < HEIGHT; i++) {
		for (j = 0; j < WIDTH; j++) {
			putchar(ch[getX(i, j)]);
		}
		putchar('\n');
	}
}

void delay(int del) {
	clock_t timer0, timer1;
	timer0 = clock();
	do {
		timer1 = clock();
	}
	while ((timer1 - timer0) < del);
}

int main() {
	char stars[HEIGHT*WIDTH];
	while (1) {
	clearArr(stars);
	fillArr(stars);
	printArr(stars);
	delay(100);
	}
	return 0;
}