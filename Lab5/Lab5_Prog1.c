#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int getWords (char* str, char* pstr[]) {
	char isWord = 0;
	int word = -1;
	while (*str) {
		if ((isWord == 0) && (*str != '\n') && (*str != ' ')) {
			isWord = 1;
			word++;
			pstr[word] = str;
		}
		if ((isWord == 1) && (*str == ' ')  && (*str != '\n')) {
			isWord = 0;
		}
		*str++;
	}
	return word+1;
}

void printWord(char* word) {
	while (*word && *word != ' ' && *word != '\n') { putchar(*word++); }
	putchar(' ');
}

int main() {
	char str[256];
	char* pstr[80];
	int numWords;
	int i, j;
	srand(time(0));
	printf("Enter string:\n");
	fflush(stdin);
	fgets(str, 256, stdin);
	numWords = getWords(str, pstr);

	for (i = 0; i < numWords; i++) {
		int ran = (rand()%(numWords-i))+i;
		printWord(pstr[ran]);
		pstr[ran] = pstr[i];
	}
	putchar('\n');
	return 0;
}