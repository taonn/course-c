#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int getWords (char* str, char* pstr[]) {
	char isWord = 0;
	int word = -1;
	while (*str) {
		if ((isWord == 0) && (*str != '\n') && (*str != ' ')) {
			isWord = 1;
			word++;
			pstr[word] = str;
		}
		if ((isWord == 1) && (*str == ' ')  && (*str != '\n')) {
			isWord = 0;
		}
		*str++;
	}
	return word+1;
}

void printWord(FILE *fw, char* word) {
	while (*word && *word != ' ' && *word != '\n') { fputc(*word++, fw); }
	fputc(' ', fw);
}


int main() {
	char str[1000];
	char *pstr[80];
	int ch = 0;
	int len = 0;
	int i;
	FILE *fp = fopen("input.txt", "rt");
	FILE *fw = fopen("output.txt", "wt");
	int numWords;
	srand(time(NULL));
	while (	fgets(str, 1000, fp) != NULL) {
		numWords = getWords(str, pstr);
		for (i = 0; i < numWords; i++) {
			int ran = (rand()%(numWords-i))+i;
			printWord(fw, pstr[ran]);
			pstr[ran] = pstr[i];
		}
	fputc('\n', fw);
	}
	fclose(fp);
	fclose(fw);
	return 0;
}